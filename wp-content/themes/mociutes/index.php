<?php get_header(); ?>

<div class="section">
    <header>
        <div class="half-circle">
            <div class="circle-text"><?php the_field('heading'); ?></div>
            <span class="ico ico-blogas"></span>
        </div>
        <h2>
            <?php
            if ( is_single() ) {
                the_title();
            } else {
                the_field('heading_icon_text');
            }
            ?>
        </h2>
    </header>

    <div class="sep"></div>

    <div class="galerija-subinfo">
        <?php the_field('galerija_sub_info') ?>
    </div>
</div>

<div class="container">
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post() ; ?>

            <div><img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'large') ?>" alt=""></div>

            <?php the_content(); ?>

            <?php if ( is_single() ): ?>
                <p><a href="javascript: history.go(-1)" class="go-back"><?php _e('Atgal','mk') ?></a></p>
            <?php endif; ?>

        <?php endwhile; ?>
    <?php endif; ?>
</div>

<?php get_footer(); ?>
