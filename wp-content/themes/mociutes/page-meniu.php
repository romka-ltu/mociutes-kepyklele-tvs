<?php get_header(); ?>

<div class="container">
	<section class="section">
		<div class="container">
			<header>
				<div class="half-circle">
					<div class="circle-text"><?php _e('Su meile ruošti produktai','mk') ?></div>
					<span class="ico ico-heart"></span>
				</div>
				<h2><?php _e('RINKITĖS PLAČIAU IŠ MŪSŲ MENIU ESANT PAGEIDAVIMUI, PRISTATYSIME JUMS TIESIAI Į NAMUS AR BIURĄ!','mk') ?></h2>
			</header>
			<div class="sep"></div>
		</div>
	</section>
</div>

<div class="container">
    <div class="prekes">
        <div class="row">
		    <?php foreach ( get_terms(['taxonomy'=>'prod_cat','hide_empty'=>false]) as $cat ) : ?>
                <div class="col-md-4">
                    <section class="preke" data-aos="fade-up">
                        <a href="<?php echo get_term_link($cat) ?>">
                            <figure><img src="<?php echo get_field('image',$cat)['sizes']['medium'] ?>" alt=""></figure>
                            <header><h2><?php echo $cat->name ?></h2></header>
                            <div class="sep sep-short"></div>
                        </a>
                    </section>
                </div>
		    <?php endforeach; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>