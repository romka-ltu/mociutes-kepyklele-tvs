<?php get_header() ?>

<?php if( have_rows('content') ): ?>
    <?php while ( have_rows('content') ) : the_row(); ?>

        <?php if( get_row_layout() == 'section' ) : ?>
            <section data-aos="fade-up" class="section">
                <div class="container">
                    <header>
                        <div class="half-circle">
                            <div class="circle-text"><?php the_sub_field('icon_text'); ?></div>
                            <span class="ico <?php the_sub_field('icon'); ?>"></span>
                        </div>
                        <h2><?php the_sub_field('heading'); ?></h2>
                    </header>
                    <div class="sep"></div>
                    <p><?php the_sub_field('text'); ?></p>
                </div>
            </section>
        <?php elseif ( get_row_layout() == 'slider' ) : ?>
            <?php $swiper_imgs = get_sub_field('images'); ?>
            <?php if ( $swiper_imgs ) : ?>
                <div class="container" data-aos="fade-right">
                    <div class="swiper-container content-slider">
                        <div class="swiper-wrapper">
                            <?php foreach( $swiper_imgs as $img ): ?>
                                <div class="swiper-slide"><img src="<?php echo $img['sizes']['large']; ?>" alt="<?php echo $img['alt']; ?>"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            <?php endif; ?>
        <?php elseif ( get_row_layout() == 'section_2_col_right' ) : ?>
            <div class="section col_2">
                <div class="container" data-aos="fade-up-right">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="sq">
                                <img src="<?php echo get_sub_field('image')['sizes']['large']; ?>" alt="<?php the_sub_field('heading'); ?>">
                            </div>
                        </div>
                        <div class="col-md-7 offset-md-1">
                            <h2><?php the_sub_field('heading'); ?></h2>
                            <p><?php the_sub_field('paragraph'); ?></p>
                            <div class="sep sep-short"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php elseif ( get_row_layout() == 'section_2_col_left' ) : ?>
            <div class="section col_2">
                <div class="container" data-aos="fade-up-left">
                    <div class="row">
                        <div class="col-md-7">
                            <h2><?php _e('AUKŠČIAUSIA KOKYBĖ','mk') ?></h2>
                            <p><?php the_sub_field('paragraph'); ?></p>
                            <div class="sep sep-short"></div>
                        </div>
                        <div class="col-md-4 offset-md-1">
                            <div class="sq">
                                <img src="<?php echo get_sub_field('image')['sizes']['large']; ?>" alt="<?php the_sub_field('heading'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer() ?>
