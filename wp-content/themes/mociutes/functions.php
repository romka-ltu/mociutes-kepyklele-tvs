<?php

define('BASE_URL', get_site_url());
define('AJAX_URL', get_site_url().'/wp-admin/admin-ajax.php');
define('ASSETS_URL', get_template_directory_uri().'/assets');
define('IMG_URL', ASSETS_URL.'/img');
define('CSS_URL', ASSETS_URL.'/css');
define('JS_URL', ASSETS_URL.'/js');
define('VER_DEV', time());
define('VER_PROD', '1.00');

add_action( 'after_setup_theme', function(){
	add_theme_support( 'menus' );
	add_theme_support( 'html5' );
	add_theme_support( 'post-thumbnails' );
} );

// Styles and scripts
add_action( 'wp_enqueue_scripts', function() {

	// Styles
	wp_register_style( 'swiper', ASSETS_URL . '/vendor/swiper/css/swiper.min.css', [], VER_PROD, 'all' );
	wp_enqueue_style( 'swiper' );

	wp_register_style( 'aos', ASSETS_URL . '/vendor/aos/css/aos.css', [], VER_PROD, 'all' );
	wp_enqueue_style( 'aos' );

	wp_register_style( 'app', CSS_URL . '/app.css', ['swiper','aos'], VER_DEV, 'all' );
	wp_enqueue_style( 'app' );

	// Scripts
	//wp_deregister_script('jquery');
	//wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, VER_PROD);
	//wp_enqueue_script('jquery');

	if ( is_archive() || is_singular('galerijos') ) {
		wp_register_script( 'fancybox', ASSETS_URL . '/vendor/fancybox/js/jquery.fancybox.min.js', ['jquery'], VER_PROD, true);
		wp_enqueue_script( 'fancybox' );

		wp_register_style( 'fancybox', ASSETS_URL . '/vendor/fancybox/css/jquery.fancybox.min.css', [], VER_PROD, 'all' );
		wp_enqueue_style( 'fancybox' );
	}

	if ( is_page(35) ) {
		wp_register_script( 'map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAUUURMpx5gkCU3U_hArRmpzmno_1oCEwo', [], VER_PROD, true);
		wp_enqueue_script( 'map' );
	}

	if ( is_singular('galerijos') ) {
		wp_register_script( 'lazy', ASSETS_URL . '/vendor/lazysizes.min.js', [], VER_PROD, true);
		wp_enqueue_script( 'lazy' );
	}

	wp_register_script( 'lettering', ASSETS_URL . '/vendor/jquery.lettering.js', ['jquery'], VER_PROD, true);
	wp_enqueue_script( 'lettering' );

	wp_register_script( 'circletype', ASSETS_URL . '/vendor/circletype.min.js', ['lettering'], VER_PROD, true);
	wp_enqueue_script( 'circletype' );

	wp_register_script( 'swiper', ASSETS_URL . '/vendor/swiper/js/swiper.jquery.min.js', ['jquery'], VER_PROD, true);
	wp_enqueue_script( 'swiper' );

	wp_register_script( 'aos', ASSETS_URL . '/vendor/aos/js/aos.js', ['jquery'], VER_PROD, true);
	wp_enqueue_script( 'aos' );

	wp_register_script( 'app', JS_URL . '/app.js', ['circletype','swiper'], VER_DEV, true);
	wp_enqueue_script( 'app' );

});

add_action( 'init', function(){
	register_nav_menus(
		array(
			'main-menu-left' => __( 'Main menu left','mk' ),
			'main-menu-right' => __( 'Main menu right','mk' ),
			'mobile-menu-main' => __( 'Mobile main menu','mk' ),
			'footer-menu' => __( 'Footer menu','mk' ),
		)
	);
} );

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Temos nustatymai',
		'menu_title'	=> 'Temos nustatymai',
		'menu_slug' 	=> 'temos-nustatymai',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

// Google maps API key for ACF Pro
add_filter('acf/settings/google_api_key', function () {
	return 'AIzaSyAUUURMpx5gkCU3U_hArRmpzmno_1oCEwo';
});

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

function cptui_register_my_cpts() {

	/**
	 * Post Type: Produktai.
	 */

	$labels = array(
		"name" => __( 'Produktai', 'mk' ),
		"singular_name" => __( 'Produktas', 'mk' ),
	);

	$args = array(
		"label" => __( 'Produktai', 'mk' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "meniu", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://mociuteskepyklele.dev/wp-content/uploads/2017/06/pie.png",
		"supports" => array( "title", "editor", "thumbnail", "revisions" ),
		"taxonomies" => array( "prod_cat" ),
	);

	register_post_type( "produktai", $args );

	/**
	 * Post Type: Galerijos.
	 */

	$labels = array(
		"name" => __( 'Galerijos', 'mk' ),
		"singular_name" => __( 'Galerija', 'mk' ),
	);

	$args = array(
		"label" => __( 'Galerijos', 'mk' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "galerijos", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-format-gallery",
		"supports" => array( "title", "thumbnail" ),
	);

	register_post_type( "galerijos", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Kategorijos.
	 */

	$labels = array(
		"name" => __( 'Kategorijos', 'mk' ),
		"singular_name" => __( 'Kategorija', 'mk' ),
	);

	$args = array(
		"label" => __( 'Kategorijos', 'mk' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Kategorijos",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'produktai', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "prod_cat", array( "produktai" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes' );
