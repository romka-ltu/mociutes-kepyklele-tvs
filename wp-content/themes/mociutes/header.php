<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Pacifico|Ubuntu:400,700&amp;subset=latin-ext" rel="stylesheet">
	<title><?php wp_title('&raquo;',true,'right'); ?></title>
	<?php wp_head() ?>
</head>
<body <?php body_class() ?>>

<header class="header">
    <div class="logo">
        <a href="<?php echo BASE_URL ?>">
            <img src="<?php echo IMG_URL ?>/logo-red.svg" alt="">
        </a>
    </div>
    <div class="mobile-menu-wrap hidden-md-up">
	    <div>
		    <?php wp_nav_menu( array( 'theme_location' => 'mobile-menu-main' ) ); ?>
        </div>
    </div>
    <div class="container hidden-sm-down">
        <div class="row">
            <div class="col-md-6">
                <div class="menu-left">
	                <?php wp_nav_menu( array( 'theme_location' => 'main-menu-left' ) ); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="menu-right">
	                <?php wp_nav_menu( array( 'theme_location' => 'main-menu-right' ) ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom"></div>
</header>

<section class="slider" style="background-image: url(<?php echo get_field('slide_1_bg','option')['sizes']['large'] ?>)">
    <h2><?php _e('Švieži pyrageliai Jūsų šeimai!','mk') ?></h2>
    <a href="<?php echo get_permalink(33) ?>" class="btn btn-main"><?php _e('žiūrėti produkciją','mk') ?></a>
</section>

<div class="kontaktai">
    <div>
        <?php _e('Turite klausimų?','mk') ?>
        <a href="tel:<?php the_field('tel','options') ?>"><?php the_field('tel','options') ?></a>
    </div>
    <div>
        <?php if (get_field('fb','option')): ?>
        <a href="<?php the_field('fb','option') ?>">
            <span class="ico ico-fb"></span>
            <div><?php _e('Sek mus!','mk') ?></div>
        </a>
        <?php endif; ?>
    </div>
</div>