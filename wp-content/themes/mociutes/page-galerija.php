<?php get_header() ?>

<div class="section">
	<header>
		<div class="half-circle">
			<div class="circle-text"><?php _e('Skanios nuotraukos','mk') ?></div>
			<span class="ico ico-galerija"></span>
		</div>
		<h2><?php _e('PASIMĖGAUKITE AKIMIRKOMIS IŠ MŪSŲ GYVENIMO','mk') ?></h2>
	</header>

	<div class="sep"></div>

	<div class="galerija-subinfo">
		<?php the_field('galerija_sub_info') ?>
	</div>
</div>

<div class="container">
    <div class="galerijos">
		<?php
		$galerijos = new WP_Query( array(
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'post_type' => 'galerijos'
		) );

		if ( $galerijos->have_posts() ) :
			while ( $galerijos->have_posts() ) : $galerijos->the_post();
				?>
                <a href="<?php echo get_permalink() ?>" data-aos="fade-up">
                    <div class="galerija" style="background-image: url(<?php the_post_thumbnail_url('large') ?>)">
                        <div class="galerija-title">
                            <h2><?php the_title() ?></h2>
                            <div>
                                <div class="half-circle"></div>
                            </div>
                        </div>
                    </div>
                </a>
				<?php
			endwhile;
			wp_reset_postdata();
		endif;
		?>
    </div>
</div>

<?php get_footer() ?>
