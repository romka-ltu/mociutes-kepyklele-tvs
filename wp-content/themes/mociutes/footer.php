    <footer class="footer">
        <div class="angleup"></div>
        <div class="logo">
            <a href="<?php BASE_URL ?>">
                <img src="<?php echo IMG_URL ?>/logo-white.svg" alt="">
            </a>
        </div>
	    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
        <div class="sep sep-short sep-white"></div>
    </footer>

	<?php wp_footer() ?>
	</body>
</html>