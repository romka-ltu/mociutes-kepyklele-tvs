<?php get_header() ?>

<section class="section">
    <div class="container">
        <header>
            <div class="half-circle">
                <div class="circle-text"><?php _e('Užsukite pas mus','mk') ?></div>
                <span class="ico ico-marker"></span>
            </div>
            <h2><?php _e('Kontaktai','mk') ?></h2>
        </header>

        <div class="sep sep-short"></div>

        <div class="kontaktai-subinfo">
            <div><?php _e('Turite klausimų?','mk') ?></div>
            <div><?php the_field('tel','options') ?></div>
            <div>
                <a href="mailto:<?php the_field('email','options') ?>">
					<?php the_field('email','options') ?>
                </a>
            </div>
        </div>

        <div class="sep sep-long"></div>

        <div class="row kontaktai-content">
            <div class="col-md-6">
				<?php the_field('info') ?>
            </div>
            <div class="col-md-6">
				<?php $location = get_field('map'); ?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            </div>
        </div>

        <div class="sep sep-long"></div>

    </div>
</section>

<?php get_footer() ?>
