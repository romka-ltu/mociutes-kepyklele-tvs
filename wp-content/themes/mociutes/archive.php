<?php get_header() ?>

<?php if ( !empty(category_description()) ) : ?>
    <div class="container">
        <div class="category-info">
            <?php echo category_description(); ?>
        </div>
    </div>
<?php endif; ?>

<div class="container">
    <section class="section">
        <div class="container">
            <header>
                <div class="half-circle">
                    <div class="circle-text"><?php the_field('heading_icon_text','term_'.get_queried_object()->term_id) ?></div>
                    <span class="ico <?php the_field('heading_icon','term_'.get_queried_object()->term_id) ?>"></span>
                </div>
                <h2><?php the_field('heading','term_'.get_queried_object()->term_id) ?></h2>
            </header>
            <div class="sep"></div>
        </div>
    </section>
</div>

<div class="container">
    <div class="row">
	    <?php if ( have_posts() ) : ?>
		    <?php while ( have_posts() ) : the_post() ; ?>
                <div class="col-md-6">
                    <div class="product">
                        <a data-fancybox data-src="#inline-<?php the_ID() ?>" href="javascript:;" data-aos="fade-up">
                            <div>
		                        <?php the_post_thumbnail('thumbnail'); ?>
                            </div>
                            <div>
                                <section>
                                    <header>
                                        <h2><?php the_title() ?></h2>
                                    </header>
			                        <?php the_content() ?>
                                    <div class="sep sep-short"></div>
                                </section>
                            </div>
                        </a>
                    </div>
                    <!-- MODAL -->
                    <div style="display: none;" class="product-inline" id="inline-<?php the_ID() ?>">
                        <div class="sep"></div>
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-6">
	                            <div><?php the_post_thumbnail('medium'); ?></div>
                            </div>
                            <div class="col-md-6">
                                <section class="section">
                                    <header>
                                        <div class="half-circle">
                                            <div class="circle-text"><?php _e('Su meile ruošti produktai','mk') ?></div>
                                            <span class="ico <?php the_field('heading_icon','term_'.get_queried_object()->term_id) ?>"></span>
                                        </div>
                                        <h2><?php the_title() ?></h2>
                                    </header>
                                    <p><?php the_content() ?></p>
                                    <footer>
                                        <?php if ( get_field('kaina') ) : ?>
			                                <?php _e('Kaina:','mk') ?> <strong><?php the_field('kaina') ?></strong>
                                        <?php endif; ?>
                                    </footer>
                                </section>
                            </div>
                        </div>
                        <div class="sep"></div>
                    </div>
                    <!-- end MODAL -->
                </div>
		    <?php endwhile; ?>
	    <?php endif; ?>
    </div>
</div>

<?php get_footer() ?>
