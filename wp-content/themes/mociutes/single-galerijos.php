<?php get_header() ?>

<div class="section">
	<div class="container">
        <header>
            <div class="half-circle">
                <div class="circle-text"><?php _e('Skanios nuotraukos','mk') ?></div>
                <span class="ico ico-galerija"></span>
            </div>
            <h2><?php the_title() ?></h2>
        </header>

        <div class="sep"></div>

        <div class="galerija-subinfo">
			<?php the_field('aprasymas') ?>
        </div>
    </div>
</div>

<div class="nuotraukos">
	<div class="container">
		<div class="row">
            <?php

            if( get_field('galerija') )
            {
                foreach( get_field('galerija') as $image )
                {
                    echo '<div class="col-lg-4">';
                    echo '<a data-fancybox="gallery" href="'.$image['sizes']['large'].'"><div class="img-wrap">';
                    echo '<img class="lazyload" data-src="'.$image['sizes']['medium'].'">';
                    echo '</div></a>';
                    echo '</div>';
                }
            }

            ?>
        </div>
	</div>
</div>

<?php get_footer() ?>
